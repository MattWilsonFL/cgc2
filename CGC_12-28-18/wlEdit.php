<!--HTML begins-->
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
    <title></title>
    <!--<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
    <link rel="stylesheet" href="css/style2.css">
    
  <?php 
    /*include 'css/css.html';*/
    require 'db.php';
  ?>
</head>
    
    <style> 
        
        #container
        {
            height: 690px;
        }
        
/*
        #currentInfo
        {
            position:relative;
            float: left;
            width:370px;
            height: 335px;
            border-style: solid;
            background-color: white;
            padding: 20px;
        }
*/
        
        #newInfo
        {
            position:relative;
            margin: auto;
            width:370px;
            height: 335px;
            border-style: solid;
            background-color: white;
            padding: 20px;
        }
        
        h3
        {
            text-align: center;    
        }
        
        p
        {
            color: black;
            margin: auto;
        }
        
        label
        {
            text-align: center;
        }
        
        
        
        #content
        {
            color: black;
            margin: auto;
            width: 300px;
            font-size: 17px;
        }
        
        #subContent
        {
            position:relative;
            right:250px;
            /*border-style: solid;*/
            margin:auto;
            /*background-color: white;*/
            width: 800px;
            height: 350px;
            padding: 5px;
        }
        
        #h_userEdit
        {
            font-size: 3.5rem;
        }
        
        #header1
        {
            font-size: 6rem;
        }
        
        #submitBtn
        {
            border-style: solid;
            border-color: black;
            border-width: 3px;
            height: 30px;
            font-size: 16px;
            color: black;
            width: 372px;
            right: 36px;
            position: relative;
            background-color: white;
            height: 34px;
        }
        
        td
        {
            height: 38px;
        }
        
    </style>
    
    
    
    
<body>
    
<?php
    session_start();
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $currentClient = $_SESSION['clientToEdit'];
        
        $wishToEdit = $_POST['wishToEdit'];
        $clientToEdit = $_POST['clientToEdit'];
        $quantityToEdit = $_POST['quantityToEdit'];
        $urgencyToEdit = $_POST['urgencyToEdit'];
        $descriptionToEdit = $_POST['descriptionToEdit'];
        
        if 
        (   
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['wishToEdit'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['clientToEdit'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['quantityToEdit'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['urgencyToEdit'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['descriptionToEdit'])) 
        )
        {
            $_SESSION['danger'] = true;
            $_SESSION['illegalString'] = "Special characters not allowed. Please try again!";
        }
        
        elseif 
        (
            ($wishToEdit == "") ||
            ($clientToEdit == "") ||
            ($quantityToEdit == "") ||
            ($urgencyToEdit == "") ||
            ($descriptionToEdit == "") 
        )
        {
            $_SESSION['danger'] = true;
            $_SESSION['illegalString'] = "Empty fields are not allowed. Please try again!";
        }
        else
        {
            $Tsql = "UPDATE wishlists
                    SET wish = '$wishToEdit', client = '$clientToEdit', quantity = '$quantityToEdit', urgency = '$urgencyToEdit', description = '$descriptionToEdit'  
                    WHERE client = '$currentClient'";
        
            sqlsrv_query($conn, $Tsql);
            
            $_SESSION['editMessage'] = "<strong>Current Wishlist: <q>'$wishToEdit'</q></strong> has been successfully edited!";
            $_SESSION['success'] = true;
            $_POST = array();
            
            header('Location: wlManager.php');
        }           
    }
?>
    
    
    
    
<!--CONTAINER#######################################################################################################
####################################################################################################################
####################################################################################################################-->
<div id='container'>
    <!--REQUIRES-->
    <?php
        require 'header.php';
        $client = $_SESSION['clientToEdit'];
        $Tsql = "SELECT  * 
                FROM wishlists 
                WHERE client = '$client'";
    
        $result = sqlsrv_query($conn, $Tsql);
        $row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC);
    
    /*danger alerts START*/
    if (isset($_SESSION['danger']))
    {
        ?>
            <div class="alert alert-danger" role="alert">
                <?php 
        
                    //?
                    if (isset($_SESSION['userNotFound']))
                    {
                        echo $_SESSION['userNotFound'];
                        unset($_SESSION['userNotFound']);
                    }
                    //?
            
                    if (isset($_SESSION['illegalString']))
                    {
                        echo $_SESSION['illegalString'];
                        unset($_SESSION['illegalString']);
                    }
                ?>
            </div>
                <?php
    }
        ?>
    <!--/*danger alerts STOP*/-->
    
    
    
    
    <!--USER EDIT FORMS START####################################################################
#################################################################################################
#################################################################################################-->
    <div id="content"> 
        <h3 id ='h_userEdit' class="text-center">Wishlist edit Form</h3>
        <hr>
        
            
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            <div id='subContent'> 
                <div id='newInfo'>
                    <table style="height:10px">
                        
                        <h3>Your new information</h3>
                        <hr>
                        <tr>
                            <td>Client:</td>
                            <td><input type="text" style='color: black' name="clientToEdit" id='user' value="<?php echo $row['client']?>" ></td>
                        </tr>

                        <tr>
                            <td>Wish:</td>
                            <td><input type="text" style='color: black' name="wishToEdit" id='pass' value="<?php echo $row['wish']?>" ></td>
                        </tr>
                        
                        <tr>
                            <td>Quantity:</td>
                            <td><input type="text" style='color: black' name="quantityToEdit" id='email' value="<?php echo $row['quantity']?>"></td>
                        </tr>
                        
                        <tr>
                            <td>Description:</td>
                            <td><input type="text" style='color: black' name="descriptionToEdit" id='role' value="<?php echo $row['description']?>" ></td>
                        </tr>
                        
                        <tr>
                            <td>Urgency:</td>
                            <td><input type="text" style='color: black' name="urgencyToEdit" id='age' value="<?php echo $row['urgency']?>" ></td>
                        </tr>

                    </table>

                    <?php       
                        unset($_SESSION['danger']);         
                    ?>
                    <br>
                    
                </div>			
            </div>
            
            <input type="submit" id='submitBtn' value="Edit User">
        </form>
    </div> 
    <!--USER EDIT FORMS START####################################################################
#################################################################################################
#################################################################################################-->
    
    
    
</div>
    
    
    
    
    
    <!--scripts-->
    <!--<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>-->

</body>
</html>
<!--HTML ends-->