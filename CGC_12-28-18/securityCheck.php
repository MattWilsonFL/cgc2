<?php

    require 'db.php';
    session_start();
    
    $userName = $_SESSION['userName'];
    $page = $_SESSION['page'];
    
    $Tsql = "SELECT * FROM users WHERE username = '$userName'";
    $result = sqlsrv_query($conn, $Tsql);
    $row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC);
    
    $_SESSION['role'] = $row["role"];
    $role = $row["role"];
    

     //accounts for if user is (on userCRUD page and is ADMIN), (on userCRUD page and is NOT ADMIN), (on clientCRUD page and is CM) 
     //(on clientCRUD page and is NOT CM)
    if (($page == "therapistManager") || ($page == "addUser") || ($page == "editUser") || ($page == "adminDashboard"))
    {
        if ($role == "admin")
        {
            //let the script finish and thus let control be given to page this file was called from.
        }
        
        else
        {
//            echo "admin else";
//            die;
            $_SESSION['danger'] = "true";
            $_SESSION['priviledgeError'] = "You do not have access to Administrator privalidges, if you are an Admin please sign in here.";
            header('Location: index.php');
        }
    }

    elseif (($page == "cmClientManager") || ($page == "cmEditUser") || ($page == "cmAddUser") || ($page == "cmDashboard")
             || ($page == "wlManager") || ($page == "wlEdit") || ($page == "wlAdd"))
    {
        if ($role == "cm")
        {
            $nonsense = "nonsense";
            //let the script finish and thus let control be given to page this file was called from.
        }
        
        else
        {
            $_SESSION['danger'] = "true";
            $_SESSION['priviledgeError'] = "You do not have access to case manager priviledges, if you are a case manager please sign in here.";
            header('Location: index.php');
        }
    }

    else
    {
//        echo "unknown error";
//        die;
        $_SESSION['danger'] = "True";
        $_SESSION['priviledgeError'] = "Unknown error.";
        header('Location: index.php');
    }
   
?>