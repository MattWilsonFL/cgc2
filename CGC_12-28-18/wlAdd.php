<!--HTML begins-->
<!DOCTYPE html>
<!--cm version-->
<html >
<head>
  <meta charset="UTF-8">
  <title>
  </title>
  <!--<link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
  <link rel="stylesheet" href="css/style2.css">

    
  <?php 
    require 'db.php';
  ?>
</head>
    
    <style> 
        
        #h_userAdd
        {
            font-size: 3.5 rem;
            position:relative;
            left: 300px;
        }
        
        p
        {
            color: black;
            margin: auto;
        }
        
        #content
        {
            color: black;
            margin: auto;
            width: auto;
            font-size: 17px;
        }
        
        #subContent
        {
            padding: 20px;
            border-style: solid;
            margin:auto;
            background-color: white;
            width: 300px;
        }
        
        #header1
        {
            font-size: 6rem;
        }
        
        #addBtn
        {
            color: black;
            border-style: solid;
        }
        
    </style>
    
    
    
    
<body>
    
<?php
    session_start();
    $_SESSION['page'] = "wlAdd";
    require 'securityCheck.php';
    
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $clientToAdd = $_POST['clientToAdd'];
        $wishToAdd = $_POST['wishToAdd'];
        $quantityToAdd = $_POST['quantityToAdd'];
        $descriptionToAdd = $_POST['descriptionToAdd'];
        $urgencyToAdd = $_POST['urgencyToAdd'];
        
        if 
        (   (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['clientToAdd'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['wishToAdd'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['quantityToAdd']))  ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['descriptionToAdd'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['urgencyToAdd']))
        )
        {
            $_SESSION['danger'] = true;
            $_SESSION['illegalString'] = "Special characters not allowed. Please try again!";
        }
        else
        {
            $Tsql = "INSERT INTO wishlists (client, wish, quantity, description, urgency) VALUES ('$clientToAdd', '$wishToAdd', '$quantityToAdd', '$descriptionToAdd', '$urgencyToAdd');";
            
            sqlsrv_query($conn, $Tsql);
            
            $_SESSION['addMessage'] = "<strong>Wishlist of Client: <q>'$clientToAdd'</q></strong> successfully added to the system!";
            $_SESSION['success'] = true;
            $_POST = array();
            
            header('Location: wlManager.php');
        }
        
        
    }
?>
    

    
    
<!--CONTAINER#######################################################################################################
####################################################################################################################
####################################################################################################################-->
<div id='container'>
    <!--REQUIRES-->
    <?php
        require 'header.php';
    
        if (isset($_SESSION['danger']))
        {
            ?>
                <div class="alert alert-danger" role="alert">
                    <?php     
                        if (isset($_SESSION['userNotFound']))
                        {
                            echo $_SESSION['userNotFound'];
                            unset($_SESSION['userNotFound']);
                        }

                        if (isset($_SESSION['illegalString']))
                        {
                            echo $_SESSION['illegalString'];
                            unset($_SESSION['illegalString']);
                        }
                    ?>
                </div>
                    <?php
            unset($_SESSION['danger']);
        }
            ?>
    
    
    <div id="content"> 
        <h3  id='h_userAdd' class="text-center">Add Wishlist</h3>
        <br><br>
            <div id='subContent'>
                
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

                    <table>
                        <tr>
                            <td>Client:</td>
                            <td><input type="text" style='color: black' name="clientToAdd" id='user' ></td>
                        </tr>

                        <tr>
                            <td>Wish:</td>
                            <td><input type="text" style='color: black' name="wishToAdd" id='pass' ></td>
                        </tr>
                        
                        <tr>
                            <td>Quantity:</td>
                            <td><input type="text" style='color: black' name="quantityToAdd" id='email' ></td>
                        </tr>
                        
                        <tr>
                            <td>Description:</td>
                            <td><input type="text" style='color: black' name="descriptionToAdd" id='gender' ></td>
                        </tr>
                        
                        <tr>
                            <td>Urgency:</td>
                            <td><input type="text" style='color: black' name="urgencyToAdd" id='gender' ></td>
                        </tr>

                    </table>

                    <br>
                    <input id='addBtn' type="submit"  value="Add Wishlist">
                </form>				
            </div>
    </div> 
</div>
    
    
    
    
    <!--scripts-->
    <!--<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>-->

</body>
</html>
<!--HTML ends-->