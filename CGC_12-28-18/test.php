<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.search-box input[type="text"]').on("keyup input", function(){
        /* Get input value on change */
        var inputVal = $(this).val();
        var resultDropdown = $(this).siblings(".result");
        if(inputVal.length){
            $.get("backend-search.php", {term: inputVal}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    
    // Set search input value on click of result item
    $(document).on("click", ".result p", function(){
        $(this).parents(".search-box").find('input[type="text"]').val($(this).text());
        $(this).parent(".result").empty();
    });
});
</script>
</head>
<body>
    <div class="search-box">
        <input type="text" autocomplete="off" placeholder="Search country..." />
        <div class="result"></div>
    </div>
    
    
    
            <!--current user table for editClient file-->
    
    
    
    <!--
                <div id='currentInfo'>
                    <table style="width:100%">
                        <h3>Your current information</h3>
                        <tr>
                            <td>Username:   </td>
                            <td><?php echo ?></td>
                        </tr>

                        <tr>
                            <td>Password:   </td>
                            <td><?php echo $row['password']?></td>
                        </tr>
                        
                        <tr>
                            <td>Email:   </td>
                            <td><?php echo $row['email']?></td>
                        </tr>
                        
                        <tr>
                            <td>Role:   </td>
                            <td><?php echo $row['role']?></td>
                        </tr>
                        
                        <tr>
                            <td>Age:   </td>
                            <td><?php echo $row['age']?></td>
                        </tr>
                        
                        <hr>

                    </table>
                </div>
-->
    
    
        <!--current user table for cmEditClient file-->
    
    
<!--
        <div id='currentInfo'>
                    <table style="width:100%">
                        <h3>Your current information</h3>
                        <tr>
                            <td>Email:   </td>
                            <td><?php echo $row['email']?></td>
                        </tr>

                        <tr>
                            <td>First Name:   </td>
                            <td><?php echo $row['fName']?></td>
                        </tr>
                        
                        <tr>
                            <td>Last Name:   </td>
                            <td><?php echo $row['lName']?></td>
                        </tr>
                        
                        <tr>
                            <td>Gender:   </td>
                            <td><?php echo $row['gender']?></td>
                        </tr>
                        
                        <tr>
                            <td>Age:   </td>
                            <td><?php echo $row['age']?></td>
                        </tr>
                        
                        <tr>
                            <td>City:   </td>
                            <td><?php echo $row['city']?></td>
                        </tr>
                        
                        <hr>

                    </table>
                </div>-->
