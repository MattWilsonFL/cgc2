<!--HTML begins-->
<!DOCTYPE html>
<!--cm version-->
<html >
<head>
  <meta charset="UTF-8">
  <title>
  </title>
  <!--<link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
  <link rel="stylesheet" href="css/style2.css">

    
  <?php 
    require 'db.php';
  ?>
</head>
    
    <style> 
        
        #h_userAdd
        {
            font-size: 3.5 rem;
            position:relative;
            left: 300px;
        }
        
        p
        {
            color: black;
            margin: auto;
        }
        
        #content
        {
            color: black;
            margin: auto;
            width: auto;
            font-size: 17px;
        }
        
        #subContent
        {
            padding: 20px;
            border-style: solid;
            margin:auto;
            background-color: white;
            width: 550px;
        }
        
        #header1
        {
            font-size: 6rem;
        }
        
        #addBtn
        {
            color: black;
            border-style: solid;
        }
        
    </style>
    
    
    
    
<body>
    
<?php
    session_start();
    $_SESSION['page'] = "cmAddUser";
    require 'securityCheck.php';
    
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $fNameToAdd = $_POST['fNameToAdd'];
        $lNameToAdd = $_POST['lNameToAdd'];
        $cityToAdd = $_POST['cityToAdd'];
        $emailToAdd = $_POST['emailToAdd'];
        $ageToAdd = $_POST['ageToAdd'];
        $genderToAdd = $_POST['genderToAdd'];
        
        if 
        (   (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['fNameToAdd'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['lNameToAdd'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['cityToAdd']))  ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['emailToAdd'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['ageToAdd']))   ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['genderToAdd']))
        )
        {
            $_SESSION['danger'] = true;
            $_SESSION['illegalString'] = "Special characters not allowed. Please try again!";
        }
        else
        {
            $Tsql = "INSERT INTO clients (fName, lName, city, email, age, gender) VALUES ('$fNameToAdd', '$lNameToAdd', '$cityToAdd', '$emailToAdd', '$ageToAdd', '$genderToAdd');";
            
            sqlsrv_query($conn, $Tsql);
            
            $_SESSION['addMessage'] = "<strong>Client: <q>'$emailToAdd'</q></strong> successfully added to the system!";
            $_SESSION['success'] = true;
            $_POST = array();
            
            header('Location: cmClientManager.php');
        }
        
        
    }
?>
    

    
    
<!--CONTAINER#######################################################################################################
####################################################################################################################
####################################################################################################################-->
<div id='container'>
    <!--REQUIRES-->
    <?php
        require 'header.php';
    
        if (isset($_SESSION['danger']))
        {
            ?>
                <div class="alert alert-danger" role="alert">
                    <?php     
                        if (isset($_SESSION['userNotFound']))
                        {
                            echo $_SESSION['userNotFound'];
                            unset($_SESSION['userNotFound']);
                        }

                        if (isset($_SESSION['illegalString']))
                        {
                            echo $_SESSION['illegalString'];
                            unset($_SESSION['illegalString']);
                        }
                    ?>
                </div>
                    <?php
            unset($_SESSION['danger']);
        }
            ?>
    
    
    <div id="content"> 

        <h3  id='h_userAdd' class="text-center">Add Client</h3>
        
            <div id='subContent'>
                
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

                    <table>
                        <tr>
                            <td>First Name:</td>
                            <td><input type="text" style='color: black' name="fNameToAdd" id='user' ></td>
                        </tr>

                        <tr>
                            <td>Last Name:</td>
                            <td><input type="text" style='color: black' name="lNameToAdd" id='pass' ></td>
                        </tr>
                        
                        <tr>
                            <td>Age:</td>
                            <td><input type="text" style='color: black' name="ageToAdd" id='email' ></td>
                        </tr>
                        
                        <tr>
                            <td>Gender:</td>
                            <td><input type="text" style='color: black' name="genderToAdd" id='gender' ></td>
                        </tr>
                        
                        <tr>
                            <td>City:</td>
                            <td><input type="text" style='color: black' name="cityToAdd" id='gender' ></td>
                        </tr>
                        
                        <tr>
                            <td>Email:</td>
                            <td><input type="text" style='color: black' name="emailToAdd" id='gender' ></td>
                        </tr>

                    </table>

                    <br>
                    <input id='addBtn' type="submit"  value="Add Client">
                </form>				
            </div>
    </div> 
</div>
    
    
    
    
    <!--scripts-->
    <!--<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>-->

</body>
</html>
<!--HTML ends-->