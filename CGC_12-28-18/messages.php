<?php
        session_start();

        if (isset($_SESSION['success']))
        {

?>
            <div class="alert alert-success" id='alertArea'>
<?php

                    if(isset($_SESSION['deleteMessage']))  
                    {
                        echo $_SESSION['deleteMessage'];
                        unset($_SESSION['deleteMessage']);
                    }

                    if(isset($_SESSION['addMessage']))  
                    {
                        echo $_SESSION['addMessage'];
                        unset($_SESSION['addMessage']);
                    }

                    if(isset($_SESSION['editMessage']))  
                    {
                        echo $_SESSION['editMessage'];
                        unset($_SESSION['editMessage']);
                    }

                    if(isset($_SESSION['searchMessage']))  
                    {
                        echo $_SESSION['searchMessage'];
                        unset($_SESSION['searchMessage']);
                    }
?>
            </div>
<?php
                    unset($_SESSION['success']);
        }
        /*success alerts stop*/
        

        
        /*danger alerts start*/
        if (isset($_SESSION['danger']) || isset($_SESSION['priviledgeError']))
        {
?>
            <div class="alert alert-danger" role="alert">
<?php
                    
                    if (isset($_SESSION['userNotFound']))
                    {
                        echo $_SESSION['userNotFound'];
                        unset($_SESSION['userNotFound']);
                    }
            
                    if (isset($_SESSION['illegalString']))
                    {
                        echo $_SESSION['illegalString'];
                        unset($_SESSION['illegalString']);
                    }
            
                    if (isset($_SESSION['adminDelete']))
                    {
                        echo $_SESSION['adminDelete'];
                        unset($_SESSION['adminDelete']);
                    }
                                         
                    if (isset($_SESSION['priviledgeError']))
                    {

                        echo $_SESSION['priviledgeError'];
                        unset($_SESSION['priviledgeError']);
                    }
?>
            </div>
<?php
            unset($_SESSION['danger']);
        }
?>