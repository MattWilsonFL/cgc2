    <!--HTML begins##################################################################################-->
    <!--#################################################################################################
    #################################################################################################-->

    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Welcome <?= $userName ?></title>
        <link rel="stylesheet" href="css/style2.css">
    
        <style>
        </style>
    </head>

    <body>

        <!--container###############################################################################################################
    ################################################################################################################################
    ################################################################################################################################-->
    <div id='container'>
<?php
    session_start();
    require 'header.php';
    require 'db.php';
    
    //gets the role of the username in question
    $userName = $_SESSION['userName'];
    $Tsql = "SELECT * FROM users WHERE username = '$userName'";
    $result = sqlsrv_query($conn, $Tsql);
    $row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC);
    $role = $row["role"];
?>

        <!--PROFILE form-->
        <div class="form">

            <h1> Welcome <?php echo $userName; ?></h1>
                
            <?php
               if ($role == 'admin')
               {
                    header("location: adminDashboard.php");
               }
            
               elseif ($role == 'cm')
               {
                    header("location: cmDashboard.php");
               } 
                
               else
               {
                    $_SESSION['danger'] = true;
                    $_SESSION['priviledgeError'] = "role not recognized! Please contact an administrator.";
                    header("location: index.php");
               }
            ?>

            <a href="logout.php"><button class="button button-block" name="logout"/>Log Out</button></a>

        </div>
    </div>




    <!--scripts-->
    <!--<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>-->

    </body>
    </html>
