<!--HTML begins-->
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
    <title></title>
    <!--<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
    <link rel="stylesheet" href="css/style2.css">
    
  <?php 
    /*include 'css/css.html';*/
    require 'db.php';
  ?>
</head>
    
    <style> 
        
        #container
        {
            height: 690px;
        }
        
/*
        #currentInfo
        {
            position:relative;
            float: left;
            width:370px;
            height: 335px;
            border-style: solid;
            background-color: white;
            padding: 20px;
        }
*/
        
        #newInfo
        {
            position:relative;
            margin: auto;
            width:500px;
            height: 370px;
            border-style: solid;
            background-color: white;
            padding: 20px;
        }
        
        h3
        {
            text-align: center;    
        }
        
        p
        {
            color: black;
            margin: auto;
        }
        
        label
        {
            text-align: center;
        }
        
        
        
        #content
        {
            color: black;
            margin: auto;
            width: 300px;
            font-size: 17px;
        }
        
        #subContent
        {
            position:relative;
            right:250px;
            /*border-style: solid;*/
            margin:auto;
            /*background-color: white;*/
            width: 800px;
            height: 350px;
            padding: 5px;
        }
        
        #h_userEdit
        {
            font-size: 3.5rem;
        }
        
        #header1
        {
            font-size: 6rem;
        }
        
        #submitBtn
        {
            border-style: solid;
            border-color: black;
            border-width: 3px;
            height: 30px;
            font-size: 16px;
            color: black;
            width: 501px;
            right: 101px;
            top: 32px;
            position: relative;
            background-color: white;
            height: 34px;
        }
        
        td
        {
            height: 38px;
        }
        
    </style>
    
    
    
    
<body>
    
<?php
    session_start();
    $_SESSION['page'] = "cmEditUser";
    require 'securityCheck.php';
    
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        
        $currentUsr = $_SESSION['clientToEdit'];
        
        $fNameToEdit = $_POST['fNameToEdit'];
        $lNameToEdit = $_POST['lNameToEdit'];
        $cityToEdit = $_POST['cityToEdit'];
        $emailToEdit = $_POST['emailToEdit'];
        $ageToEdit = $_POST['ageToEdit'];
        $genderToEdit = $_POST['genderToEdit'];
        
//        echo $fNameToEdit;
//        echo $lNameToEdit;
//        echo $cityToEdit;
//        echo $emailToEdit;
//        echo $ageToEdit;
//        echo $genderToEdit;
//        die();
        
        if 
        (   (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['fNameToEdit'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['lNameToEdit'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['cityToEdit']))  ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['emailToEdit'])) ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['ageToEdit']))   ||
            (preg_match('/[\'^£$%&*()}{#~?><>,|=_+¬-]/', $_POST['genderToEdit']))
        )
        {
            $_SESSION['danger'] = true;
            $_SESSION['illegalString'] = "Special characters not allowed. Please try again!";
        }
        else
        {
            $Tsql = "UPDATE clients
    SET fName = '$fNameToEdit', lName = '$lNameToEdit', email = '$emailToEdit', gender = '$genderToEdit', age = '$ageToEdit', city ='$cityToEdit' WHERE email = '$currentUsr'";
        
            sqlsrv_query($conn, $Tsql);
            
            $_SESSION['editMessage'] = "<strong>Current Client: <q>'$emailToEdit'</q></strong> has been successfully edited!";
            $_SESSION['success'] = true;
            $_POST = array();
            
            header('Location: cmClientManager.php');
        }           
    }
?>
    
    
    
    
<!--CONTAINER#######################################################################################################
####################################################################################################################
####################################################################################################################-->
<div id='container'>
    <!--REQUIRES-->
    <?php
        require 'header.php';
        $userName = $_SESSION['clientToEdit'];
        $Tsql = "SELECT  * 
                FROM clients
                WHERE email = '$userName'";
    
        $result = sqlsrv_query($conn, $Tsql);
        $row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC);
    
    /*danger alerts START*/
    if (isset($_SESSION['danger']))
    {
        ?>
            <div class="alert alert-danger" role="alert">
                <?php     
                    if (isset($_SESSION['userNotFound']))
                    {
                        echo $_SESSION['userNotFound'];
                        unset($_SESSION['userNotFound']);
                    }
            
                    if (isset($_SESSION['illegalString']))
                    {
                        echo $_SESSION['illegalString'];
                        unset($_SESSION['illegalString']);
                    }
                ?>
            </div>
                <?php
    }
        ?>
    <!--/*danger alerts STOP*/-->
    
    
    
    
    <!--USER EDIT FORMS START####################################################################
#################################################################################################
#################################################################################################-->
    <div id="content"> 
        <h3 id ='h_userEdit' class="text-center">User edit Form</h3>
        <hr>
        
            
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            <div id='subContent'>
                
                
                
<!--                current info went here-->
                    
                <div id='newInfo'>
                    <table style="height:10px">
                        
                        <h3>Your new information</h3>
                        <hr>
                        <tr>
                            <td>Email:</td>
                            <td><input type="text" style='color: black' name="emailToEdit" id='user' value="<?php echo $row['email']?>" ></td>
                        </tr>

                        <tr>
                            <td>First Name:</td>
                            <td><input type="text" style='color: black' name="fNameToEdit" id='fNameToEdit' value="<?php echo $row['fName']?>" ></td>
                        </tr>
                        
                        <tr>
                            <td>Last Name:</td>
                            <td><input type="text" style='color: black' name="lNameToEdit" id='lNameToEdit' value="<?php echo $row['lName']?>" ></td>
                        </tr>
                        
                        <tr>
                            <td>Gender:</td>
                            <td><input type="text" style='color: black' name="genderToEdit" id='genderToEdit' value="<?php echo $row['gender']?>" ></td>
                        </tr>
                        
                        <tr>
                            <td>Age:</td>
                            <td><input type="text" style='color: black' name="ageToEdit" id='ageToEdit' value="<?php echo $row['age']?>" ></td>
                        </tr>
                        
                        <tr>
                            <td>City:</td>
                            <td><input type="text" style='color: black' name="cityToEdit" id='cityToEdit' value="<?php echo $row['city']?>" ></td>
                        </tr>

                    </table>

                    <?php       
                        unset($_SESSION['danger']);         
                    ?>
                    <br>
                    
                </div>			
            </div>
            
            <input type="submit" id='submitBtn' value="Edit User">
        </form>
    </div> 
    <!--USER EDIT FORMS START####################################################################
#################################################################################################
#################################################################################################-->
    
    
    
</div>
    
    
    
    
    
    <!--scripts-->
    <!--<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>-->

</body>
</html>
<!--HTML ends-->